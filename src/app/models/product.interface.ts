export interface Product {
  id: number;
  title: string;
  price: string;
  imageUrl: string;
  urlProd: string;
}
